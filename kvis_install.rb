#!/usr/bin/env ruby

raise "Must run as root" unless Process.uid.zero?

pwd = Dir.pwd

remote_common = "ftp://ftp.atnf.csiro.au/pub/software/karma/karma-1.7.25-common.tar.bz2"
remote_linux64 = "ftp://ftp.atnf.csiro.au/pub/software/karma/karma-1.7.25-amd64_Linux_libc6.3.tar.bz2"

Dir.chdir("/opt")
[remote_common, remote_linux64].each do |r|
    system("wget #{r} -O #{pwd}/#{File.basename(r)}") unless File.exist?("#{pwd}/#{File.basename(r)}")
    system("tar -xf #{pwd}/#{File.basename(r)}")
end

s = File.basename(remote_linux64).split("-")
karma_version = s[0..1].join("-")
karma_system_version = s[-1].split(".")[0..-3].join(".")

env = File.read("/etc/environment")
unless env.include?("karma")
    env.sub!(/(PATH=.*)"/, "\\1:/opt/#{karma_version}/#{karma_system_version}/bin\"")
    if env.include?("LD_LIBRARY_PATH")
        env.sub!(/(LD_LIBRARY_PATH=.*)"/, "\\1:/opt/#{karma_version}/#{karma_system_version}/lib\"")
    else
        env << "LD_LIBRARY_PATH=/opt/#{karma_version}/#{karma_system_version}/lib\n"
    end
    env << "KARMABASE=/opt/#{karma_version}/#{karma_system_version}\n"
end
File.open("/etc/environment", "w") { |f| f.puts env }
