#!/usr/bin/env ruby

# Ubuntu 17.10's emacs package has dpkg problems? WTF?

raise "Must run as root" unless Process.uid.zero?

# Download the tarball, if it's not in the PWD.
remote_file = "ftp://ftp.gnu.org/gnu/emacs/emacs-26.1.tar.xz"
base = File.basename(remote_file)
stem = base.split(".tar.xz").first
system("wget #{remote_file}") unless File.exist?(base)
system("tar -xf #{base}") unless File.exist?(stem)

Dir.chdir stem
system("CFLAGS=\"-O3 -march=native\" ac_cv_lib_gif_EGifPutExtensionLast=yes ./configure \
                                    --prefix=/usr/local \
                                    --sysconfdir=/etc \
                                    --with-x-toolkit=lucid \
                                    --with-xft \
                                    --with-modules \
                                    --enable-link-time-optimization")
system("make")
system("make install")
