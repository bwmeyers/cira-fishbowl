#!/usr/bin/env ruby

raise "Must run as root" unless Process.uid.zero?

remote_file = "https://mega.nz/linux/MEGAsync/xUbuntu_16.04/amd64/megasync-xUbuntu_16.04_amd64.deb"
remote_nautilus_file = "https://mega.nz/linux/MEGAsync/xUbuntu_16.04/amd64/nautilus-megasync-xUbuntu_16.04_amd64.deb"

# Download the files, if they're not in the PWD.
system("wget #{remote_file}") unless File.exist?(File.basename(remote_file))
system("wget #{remote_nautilus_file}") unless File.exist?(File.basename(remote_nautilus_file))

system("sudo dpkg -i #{File.basename(remote_file)}")
system("sudo apt -f -y install")

system("sudo dpkg -i #{File.basename(remote_nautilus_file)}")
system("sudo apt -f -y install")
