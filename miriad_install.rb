#!/usr/bin/env ruby

raise "Must run as root" unless Process.uid.zero?

pwd = Dir.pwd

# Download the tarballs, if they're not in the PWD.
remote_common = "ftp://ftp.atnf.csiro.au/pub/software/miriad/miriad-common.tar.bz2"
remote_code = "ftp://ftp.atnf.csiro.au/pub/software/miriad/miriad-code.tar.bz2"
remote_linux64 = "ftp://ftp.atnf.csiro.au/pub/software/miriad/miriad-linux64.tar.bz2"

[remote_common, remote_code, remote_linux64].each do |r|
    system("wget #{r}") unless File.exist?(File.basename(r))
end

Dir.chdir("/opt")
system("tar -xf #{pwd}/#{File.basename(remote_common)}")
system("tar -xf #{pwd}/#{File.basename(remote_code)}")
system("tar -xf #{pwd}/#{File.basename(remote_linux64)}")

env = File.read("/etc/environment")
to_be_added_to_env = []
unless env.include?("miriad")
    env.sub!(/(PATH=.*)"/, "\\1:/opt/miriad/linux64/bin\"")

    to_be_added_to_env.push("MIR=/opt/miriad")
    to_be_added_to_env.push("MIRCAT=/opt/miriad/cat")
    to_be_added_to_env.push("MIRPDOC=/opt/miriad/doc")
    to_be_added_to_env.push("MIRINC=/opt/miriad/inc")
    to_be_added_to_env.push("MIRPROG=/opt/miriad/prog")
    to_be_added_to_env.push("MIRSUBS=/opt/miriad/subs")
    to_be_added_to_env.push("MIRMAN=/opt/miriad/linux64/man")
    to_be_added_to_env.push("MIRARCH=linux64")
    to_be_added_to_env.push("MIRARCHD=/opt/miriad/linux64")
    to_be_added_to_env.push("MIRBIN=/opt/miriad/linux64/bin")
    to_be_added_to_env.push("MIRLIB=/opt/miriad/linux64/lib")
    to_be_added_to_env.push("MIRDEF=.")
    to_be_added_to_env.push("PGPLOT_FONT=/opt/miriad/linux64/lib/grfont.dat")
end
unless to_be_added_to_env.empty?
    to_be_added_to_env.each do |m|
        env << "\n" << m
    end
    env << "\n"
    File.open("/etc/environment", "w") { |f| f.puts env }
end

Dir.chdir("/opt/miriad")
system("./configure")
