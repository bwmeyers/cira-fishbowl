#!/usr/bin/env ruby

raise "Must run as root" unless Process.uid.zero?

def add_packages(container, package_prefix, packages)
    packages.each do |p|
        container.push("#{package_prefix}-#{p}")
    end
end

core = %w[zsh emacs vim git htop ncdu openssh-server autossh rsync tcsh gfortran stow screen tmux
          curl tigervnc-viewer]

python = %w[virtualenv python python3]
python_extras = %w[dev numpy scipy astropy matplotlib h5py]
add_packages(python, "python3", python_extras)

languages = %w[ruby openjdk-8-jre]

latex = %w[texlive-full latexdiff texmaker texstudio lyx]

libraries = %w[libblas-dev liblapack-dev libhdf5-dev libreadline8 libfftw3-dev
               libcfitsio-dev pgplot5 libx11-dev libpng-dev libncurses5-dev libxt-dev
               libextutils-f77-perl wcslib-dev libxaw7-dev libgif-dev libtiff5-dev libgnutls28-dev
               xorg-dev]

astronomy = %w[swarp stellarium saods9]
casacore_extras = %w[data dev doc tools]
add_packages(astronomy, "casacore", casacore_extras)

others = %w[synaptic]

def psystem(command)
    puts command
    system("sh -c \"#{command}\"")
end

# Update and install all the packages.
psystem("apt update")
psystem("apt install -y #{(core + python + languages + latex + libraries + astronomy + others).join(" ")}")
